
CC := /home/tsisarzh/Android/android-ndk-r8e/toolchain/bin/arm-linux-androideabi-gcc

HIJACK  := hijack
PAYLOAD := payload.so

all: $(HIJACK) $(PAYLOAD)

$(HIJACK): hijack.c
	$(CC) -mthumb -march=armv7 $< -o $@

$(PAYLOAD): payload.c payload.S
	$(CC) -mthumb -march=armv7 -shared $^ -o $@ -lGLESv1_CM -llog
