#include <stdio.h>
#include <dlfcn.h>
#include <unistd.h>
#include <signal.h>
#include <sys/ptrace.h>
#include <sys/wait.h>
#include <sys/types.h>

int ptrace_push(int pid, char *addr, char *data, unsigned size)
{
	union
	{
		long value;
		char bytes[4];
	} word;

	int i;
	for(i = 0; i < size / 4; i++)
	{
		memcpy(word.bytes, data, 4);

		ptrace(PTRACE_POKEDATA, pid, addr, word.value);

		addr += 4;
		data += 4;
	}

	unsigned remain = size % 4;

	if(remain > 0)
	{
		word.value = ptrace(PTRACE_PEEKDATA, pid, addr, 0);

		for(i = 0; i < remain; i++)
			word.bytes[i] = *data++;

		ptrace(PTRACE_POKEDATA, pid, addr, word.value);
	}

	return 0;
}

int ptrace_call(int pid, unsigned addr, long *params, unsigned params_num, struct pt_regs *regs)
{
	int i;
	for(i = 0; i < params_num && i < 4; i++)
		regs->uregs[i] = params[i];

	if(i < params_num)
	{
		unsigned size = (params_num - i) * 4;

		regs->ARM_sp -= size;
		ptrace_push(pid, (void *)regs->ARM_sp, (char *)&params[i], size);
	}

	regs->ARM_pc = addr;
	regs->ARM_lr = 0;

	if(regs->ARM_pc & 1)
		regs->ARM_cpsr |=  PSR_T_BIT;
	else
		regs->ARM_cpsr &= ~PSR_T_BIT;

	ptrace(PTRACE_SETREGS, pid, 0, regs);

	ptrace(PTRACE_CONT, pid, 0, 0);
	waitpid(pid, 0, 0);

	return 0;
}

void *get_module_base_addr(pid_t pid, const char *name)
{
	FILE *fp;
	char path[32];
	long addr = -1;
	char buffer[1024];
	char *temp;

	if(pid == -1)
		snprintf(path, sizeof(path), "/proc/self/maps");
	else
		snprintf(path, sizeof(path), "/proc/%d/maps", pid);
	fp = fopen(path, "r");
	if(!fp)
		return (void *)(-1);
	while(fgets(buffer, sizeof(buffer), fp))
	{
		if(strstr(buffer, name))
		{
			temp = strtok(buffer, "-");
			addr = strtoul(temp, NULL, 16);
			if(addr == 0x8000)
			{
				addr = 0;
			}
			break;
		}
	}
	fclose(fp);

	return (void *)addr;
}

int main(int argc, char *argv[])
{
	const char *payload_path = "/data/payload.so";
	const char *payload_func = "load";

	void *dlopen_addr_local = dlsym(RTLD_DEFAULT, "dlopen");
	void *dlsym_addr_local  = dlsym(RTLD_DEFAULT, "dlsym");

	printf("dlopen_addr_local = %08x\n", dlopen_addr_local);
	printf("dlsym_addr_local = %08x\n", dlsym_addr_local);

	long params[2];
	struct pt_regs regs;
	struct pt_regs regs_original;

	int pid = atoi(argv[1]);

	void *dlopen_addr = dlopen_addr_local - get_module_base_addr(-1, "linker") + get_module_base_addr(pid, "linker");
	void *dlsym_addr  = dlsym_addr_local - get_module_base_addr(-1, "linker") + get_module_base_addr(pid, "linker");

	printf("dlopen_addr = %08x\n", dlopen_addr);
	printf("dlsym_addr = %08x\n", dlsym_addr);

	ptrace(PTRACE_ATTACH, pid, 0, 0);
	waitpid(pid, 0, 0);

	ptrace(PTRACE_GETREGS, pid, 0, &regs);
	memcpy(&regs_original, &regs, sizeof(regs));

	// dlopen payload_path
	regs.ARM_sp -= 128;
	ptrace_push(pid, regs.ARM_sp, payload_path, strlen(payload_path) + 1);
	params[0] = regs.ARM_sp;
	params[1] = RTLD_NOW;
	ptrace_call(pid, dlopen_addr, params, 2, &regs);
	ptrace(PTRACE_GETREGS, pid, 0, &regs);
	printf("%08X\n", regs.ARM_r0);

	// dlsym payload_func
	regs.ARM_sp -= 128;
	ptrace_push(pid, regs.ARM_sp, payload_func, strlen(payload_func) + 1);
	//params[0] = RTLD_DEFAULT;
	params[0] = regs.ARM_r0;
	params[1] = regs.ARM_sp;
	ptrace_call(pid, dlsym_addr, params, 2, &regs);
	ptrace(PTRACE_GETREGS, pid, 0, &regs);
	printf("%08X\n", regs.ARM_r0);
	
	// call payload_func
	ptrace_call(pid, regs.ARM_r0, 0, 0, &regs);
	ptrace(PTRACE_GETREGS, pid, 0, &regs);
	printf("%08X\n", regs.ARM_r0);

	ptrace(PTRACE_SETREGS, pid, 0, &regs_original);
	ptrace(PTRACE_CONT, pid, 0, 0);
	//waitpid(pid, 0, 0);
	ptrace(PTRACE_DETACH, pid, 0, 0);

	return 0;
}
