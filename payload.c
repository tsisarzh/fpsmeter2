#include <stdio.h>
#include <dlfcn.h>
#include <sys/types.h>
#include <linux/elf.h>

#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/mman.h>

#include <GLES/gl.h>
#include <GLES/glext.h>

#include <android/log.h>

#undef PAGE_SIZE
#undef PAGE_MASK

#define PAGE_SIZE 4096
#define PAGE_MASK (PAGE_SIZE - 1)
#define PAGE_START(x) ((x) & ~PAGE_MASK)

#define ANDROID_ARM_LINKER

struct link_map
{
	uintptr_t l_addr;
	char * l_name;
	uintptr_t l_ld;
	struct link_map * l_next;
	struct link_map * l_prev;
};

typedef struct soinfo soinfo;

#define SOINFO_NAME_LEN 128

struct soinfo
{
	const char name[SOINFO_NAME_LEN];
	Elf32_Phdr *phdr;
	int phnum;
	unsigned entry;
	unsigned base;
	unsigned size;

	int unused;  // DO NOT USE, maintained for compatibility.

	unsigned *dynamic;

	unsigned wrprotect_start;
	unsigned wrprotect_end;

	soinfo *next;
	unsigned flags;

	const char *strtab;
	Elf32_Sym *symtab;

	unsigned nbucket;
	unsigned nchain;
	unsigned *bucket;
	unsigned *chain;

	unsigned *plt_got;

	Elf32_Rel *plt_rel;
	unsigned plt_rel_count;

	Elf32_Rel *rel;
	unsigned rel_count;

	unsigned *preinit_array;
	unsigned preinit_array_count;

	unsigned *init_array;
	unsigned init_array_count;
	unsigned *fini_array;
	unsigned fini_array_count;

	void (*init_func)(void);
	void (*fini_func)(void);

#ifdef ANDROID_ARM_LINKER
	/* ARM EABI section used for stack unwinding. */
	unsigned *ARM_exidx;
	unsigned ARM_exidx_count;
#endif
	unsigned refcount;
	struct link_map linkmap;
};

static unsigned __asm_thumb32_b_w(unsigned instruction_addr, unsigned target_addr)
{
	unsigned instruction = 0;
	// S:I1:I2:imm10:imm11:0
	int offset = 0;

	offset = target_addr - instruction_addr - 4;

	unsigned S      = (offset >> 24) & 1;
	unsigned I1     = (offset >> 23) & 1;
	unsigned I2     = (offset >> 22) & 1;
	unsigned imm10  = (offset >> 12) & 0x3ff;
	unsigned imm11  = (offset >> 1 ) & 0x7ff;
	unsigned J1     = (I1 == S);
	unsigned J2     = (I2 == S);
	unsigned opcode = 0xf0009000;

	instruction = opcode | (S << 26) | (imm10 << 16) | (J1 << 13) | (J2 << 11) | imm11;

	return instruction;
}

static unsigned get_rodata_addr(const char *path)
{
	unsigned result = 0;

	int fd = open(path, O_RDONLY);

	Elf32_Ehdr eh[1];

	read(fd, (void *)eh, sizeof(eh));

	Elf32_Shdr sh[1];
	int offset;

	offset = eh->e_shoff + eh->e_shentsize * eh->e_shstrndx;
	lseek(fd, offset, SEEK_SET);
	read(fd, (void *)sh, sizeof(sh));

	char *strtab = (char *)malloc(sh->sh_size);

	offset = sh->sh_offset;
	lseek(fd, offset, SEEK_SET);
	read(fd, (void *)strtab, sh->sh_size);

	lseek(fd, eh->e_shoff, SEEK_SET);
	int i;
	for(i = 0; i < eh->e_shnum; i++)
	{
		read(fd, (void *)sh, sizeof(sh));

		if(!strcmp(strtab + sh->sh_name, ".rodata"))
		{
			result = sh->sh_offset;

			break;
		}
	}

	free(strtab);

	close(fd);

	return result;
}

void postFramebuffer_payload()
{
	__android_log_print(ANDROID_LOG_VERBOSE, "payload.so", "postFramebuffer_payload");
}

void postFramebuffer_wrapper();

unsigned short postFramebuffer_code[10];

struct trampoline
{
	unsigned short hw_1;
	unsigned short hw_2;
	unsigned target;
};

#define STMDB_HW_1 0xe92d
// ldr.w pc, [pc]
#define LDRPC_HW_1 0xf8df
#define LDRPC_HW_2 0xf000

#define MPROTECT(addr) mprotect((void *)PAGE_START((unsigned)addr), sizeof(unsigned), PROT_READ | PROT_WRITE | PROT_EXEC)

int load()
{
	soinfo *elf = dlopen("/system/lib/libsurfaceflinger.so", 0);

	unsigned postFramebuffer_addr;

	postFramebuffer_addr = (unsigned)dlsym(elf, "_ZN7android14SurfaceFlinger15postFramebufferEv");
	postFramebuffer_addr--;
	__android_log_print(ANDROID_LOG_VERBOSE, "payload.so", "dlsym postFramebuffer addr = %08x\n", postFramebuffer_addr);

	MPROTECT(postFramebuffer_addr);

	unsigned short *addr = (unsigned short *)postFramebuffer_addr;
	__android_log_print(ANDROID_LOG_VERBOSE, "payload.so", "postFramebuffer_addr = %08x\n", addr);

	int i;
	for(i = 0; i < 10; i++)
	{
		__android_log_print(ANDROID_LOG_VERBOSE, "payload.so", "postFramebuffer value by addr = %08x\n", *addr);

		if(*addr != STMDB_HW_1)
			addr++;
		else
			break;
	}

	__android_log_print(ANDROID_LOG_VERBOSE, "payload.so", "STMDB_HW_1 addr = %08x\n", addr);

	MPROTECT(postFramebuffer_code);

	// code before STMDB + STMDB
	unsigned size = i * 2 + 4;
	memcpy((void *)postFramebuffer_code, (void *)postFramebuffer_addr, size);

	struct trampoline *trampoline;

	trampoline = (struct trampoline *)&postFramebuffer_code[i + 2];
	trampoline->hw_1   = LDRPC_HW_1;
	trampoline->hw_2   = LDRPC_HW_2;
	trampoline->target = postFramebuffer_addr + size + 1;

	for(i = 0; i < 10; i++)
		__android_log_print(ANDROID_LOG_VERBOSE, "payload.so", "%04x\n", postFramebuffer_code[i]);

	unsigned rodata_addr = elf->base + get_rodata_addr("/system/lib/libsurfaceflinger.so");
	__android_log_print(ANDROID_LOG_VERBOSE, "payload.so", "%08x\n", rodata_addr);

	MPROTECT(rodata_addr);

	trampoline = (struct trampoline *)rodata_addr;
	trampoline->hw_1   = LDRPC_HW_1;
	trampoline->hw_2   = LDRPC_HW_2;
	trampoline->target = (unsigned)postFramebuffer_wrapper;

	unsigned instruction = __asm_thumb32_b_w(postFramebuffer_addr, rodata_addr);
	instruction = (instruction >> 16) | (instruction << 16);

	*((unsigned *)postFramebuffer_addr) = instruction;

	return 0;
}

int unload()
{
	return 0;
}
